package com.sid.partycashback.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import lombok.Data;

@Entity
@Data
public class PartyPeople {

    @PrimaryKey
    int id;

    @ColumnInfo(name = "party_id")
    int party_id;

}
