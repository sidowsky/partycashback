package com.sid.partycashback.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

import lombok.Data;

@Data
@Entity
public class Party {
    @PrimaryKey
    private int id;

    @ColumnInfo(name = "party_name")
    private String partyName;

    @ColumnInfo(name = "party_date")
    private Date partyDate;

    @ColumnInfo(name = "amount_of_money")
    private double moneySpent;
}
