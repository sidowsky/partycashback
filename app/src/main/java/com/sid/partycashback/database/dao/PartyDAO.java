package com.sid.partycashback.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.sid.partycashback.database.Party;

import java.util.List;

@Dao
public interface PartyDAO {

    @Query("SELECT * FROM Party")
    List<Party> getAll();

    @Query("SELECT * FROM Party WHERE id IN (:userIds)")
    List<Party> loadAllByIds(int[] userIds);

    @Insert
    void insertParty(Party party);

}
