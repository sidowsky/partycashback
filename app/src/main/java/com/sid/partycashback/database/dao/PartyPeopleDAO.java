package com.sid.partycashback.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PartyPeopleDAO {
    @Query("SELECT id FROM partypeople WHERE party_id = :partyID")
    List<Integer>loadUserIdByParty(int partyID);

    @Insert
    void addPeopleToParty(int partyID, int[] usersID);
}
