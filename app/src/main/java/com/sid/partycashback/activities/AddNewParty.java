package com.sid.partycashback.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;


import com.sid.partycashback.R;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddNewParty extends FragmentActivity implements DatePickerDialog.OnDateSetListener {

    Date dateToSave;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_party);


        final TextView partyName = (TextView)findViewById(R.id.partyName);
        final TextView partyAmountOfCash = (TextView)findViewById(R.id.partyAmountOfCash);

        Button savePartyButton = (Button) findViewById(R.id.saveNewPartyButton);
        Button setDateButton = (Button) findViewById(R.id.setDateButton);

        setDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });
        savePartyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CharSequence partyNameText = partyName.getText();
                Integer wastedCash = Integer.getInteger(partyAmountOfCash.getText().toString());


                /*Intent intent = new Intent(); // todo tu dodac element do bazy danych i powrócić do poprzedniej aktywności
                intent.putExtra("keyName", stringToPassBack);
                setResult(RESULT_OK, intent);
                finish();
                */

                // todo take data form fields, create row id db and return to main activity
            }
        });
    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

        Button clickedButton = (Button)findViewById(R.id.setDateButton);
        clickedButton.setText(currentDateString);
        dateToSave = calendar.getTime();
    }

}
